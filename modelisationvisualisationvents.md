---
version: 2
titre: Modélisation et visualisation de vents dans une vallée
type de projet: Projet de semestre 6
année scolaire: 2021/2022
abréviation: ModelVis Vents
filières:
  - Télécommunications
  - Informatique
nombre d'étudiants: 2
professeurs co-superviseurs:
  - Richard Baltensperger
mots-clé: [modélisation, visualisation, vents, réalité virtuelle]
langue: [F]
confidentialité: non
suite: non
---

\begin{center}
\includegraphics[width=0.7\textwidth]{img/espaceballon.jpg}
\end{center}

## Contexte/Objectifs
Espace Ballon (https://www.espace-ballon.ch) est une fondation située à Château d’Oex dont l’objectif est de promouvoir les ballons à air chaud ainsi que la région. 

Pour augmenter leur offre d’activités dans leurs locaux, Espace Ballon désire disposer d’un simulateur virtuel 3D de ballon à air chaud de niveau professionnel qui permet au public de s’initier au pilotage et aux professionnels d’effectuer des heures de vol d’entrainement.

Pour atteindre une qualité professionnelle du simulateur, il est nécessaire de disposer de modélisations très réalistes des différentes situations des vents dans la vallée du Pays-d’Enhaut. 

WindNinja est un programme informatique qui calcule des champs de vents variant dans l'espace pour des applications liées aux incendies de forêt.

Dans ce projet, nous désirons évaluer si WindNinja est adapté pour obtenir des modélisations réalistes des vents dans une vallée.

Les étapes de ce projet sont 

-  Analyser le logiciel WindNinja
-  Utiliser WindNinja avec des données météorologiques fournies
-  Développer une visualisation des vents en réalité virtuelle qui affiche les résultats de WindNinja et permet une analyse aisée et globale des vents
-  Modifier les paramètres d’entrée de WindNinja pour introduire des phénomènes aérologiques locaux et vérifier que les modélisations obtenues correspondent aux attentes

Pour ce projet, nous disposons de données de vols de ballon à air chaud ainsi que de cartes de vents, élaborées par un pilote professionnel qui connait précisément les différentes situations de vents de la région.

Le projet peut être poursuivi en travail de bachelor.



## Tâches
Travaux à réaliser:

-  Elaborer le cahier des charges en étroite collaboration avec les responsables du projet.
-  Prendre en main le logiciel WindNinja
-  Définir une visualisation des vents en réalité virtuelle qui permet une analyse aisée et globale des vents
-  Développer la visualisation
-  Modifier les paramètres d’entrée de WindNinja pour introduire des phénomènes aérologiques locaux et vérifier que les modélisations obtenues correspondent aux attentes
-  Elaborer une documentation technique complète et rédaction d’un rapport décrivant la démarche et les choix opérés.
